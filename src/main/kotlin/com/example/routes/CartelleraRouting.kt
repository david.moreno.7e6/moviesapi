package com.example.routes

import com.example.model.Comentari
import com.example.model.Pelicula
import com.example.model.cartellera
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.cartelleraRoute(){
    route("/peliculas") {
        get {
            if (cartellera.isNotEmpty()) call.respond(cartellera)
            else call.respondText("No s'han trovat pel·licules.", status = HttpStatusCode.OK)
        }
        get("{id?}"){
            if (call.parameters["id"].isNullOrBlank()){
                return@get call.respondText("L 'id de la pelicula no es correcte.", status = HttpStatusCode.BadRequest)
            }
            for (pelicula in cartellera){
                if(pelicula.id == call.parameters["id"]) return@get call.respond(pelicula)
            }
            call.respondText("No s'ha trovat l'id ${call.parameters["id"]} en la cartellera.", status = HttpStatusCode.NotFound)
        }
        post {
            val pelicula = call.receive<Pelicula>()
            pelicula.id = (cartellera.size + 1).toString()
            cartellera.add(pelicula)
            call.respondText("Pel·licula afegida correctament", status = HttpStatusCode.Created)
        }
        post("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@post call.respondText("L 'id de la pelicula no es correcte.", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (pelicula in cartellera) {
                if (pelicula.id == id) {
                    val comentari = call.receive<Comentari>()
                    comentari.id = (pelicula.comentaris.size + 1).toString()
                    comentari.id_pelicula = id
                    pelicula.comentaris.add(comentari)
                    return@post call.respondText("Comentari afegit correctament a la pel·licula ${pelicula.titol}",status = HttpStatusCode.Created )
                }
            }
            call.respondText("No s'ha trovat l'id $id en la cartellera.", status = HttpStatusCode.NotFound)
        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@put call.respondText("L 'id de la pelicula no es correcte.", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            val peliculaPerModificar = call.receive<Pelicula>()
            for (pelicula in cartellera) {
                if (pelicula.id == id) {
                    pelicula.titol = peliculaPerModificar.titol
                    pelicula.any = peliculaPerModificar.any
                    pelicula.genere = peliculaPerModificar.genere
                    pelicula.director = peliculaPerModificar.director
                    return@put call.respondText(
                        "S'ha actualitzat la pel·licula ${pelicula.titol}",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText("No s'ha trovat l'id $id en la cartellera.", status = HttpStatusCode.NotFound)
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@delete call.respondText("L 'id de la pelicula no es correcte.", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (pelicula in cartellera) {
                if (pelicula.id == id) {
                    cartellera.remove(pelicula)
                    return@delete call.respondText("S'ha esborrat la pel·licula ${pelicula.titol}", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText("No s'ha trovat l'id $id en la cartellera., status = HttpStatusCode.NotFound")
        }
        get("{id?}/comentaris"){
            if (call.parameters["id"].isNullOrBlank()){
                return@get call.respondText("L 'id de la pelicula no es correcte.", status = HttpStatusCode.BadRequest)
            }
            for (pelicula in cartellera){
                if(pelicula.id == call.parameters["id"]){
                    return@get call.respond(pelicula.comentaris)
                }
            }
            call.respondText("No s'han trovat comentaris per l'id ${call.parameters["id"]} en la cartellera.", status = HttpStatusCode.NotFound)
        }



    }
}