package com.example.model

import kotlinx.serialization.Serializable
import java.sql.Timestamp
import java.time.LocalDateTime

@Serializable
data class Comentari(
    var id: String,
    var id_pelicula: String,
    val comentari: String,
    var data_creacio: String
){
    init {
        data_creacio= LocalDateTime.now().toString()
    }
}